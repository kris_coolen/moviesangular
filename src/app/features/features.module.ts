import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { MoviesDetailComponent } from './movies-detail/movies-detail.component';
import { MoviesAddComponent } from './movies-add/movies-add.component';
import { FormsModule } from '@angular/forms';





@NgModule({
  declarations: [MoviesListComponent, MoviesDetailComponent, MoviesAddComponent],
  imports: [
    CommonModule, BrowserModule, AppRoutingModule, FormsModule
  ],
  exports: [MoviesListComponent, MoviesDetailComponent, MoviesAddComponent]
})
export class FeaturesModule { }
