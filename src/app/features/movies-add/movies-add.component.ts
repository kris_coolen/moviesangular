import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/core/services/movie-data/movies.service';
import { Movie } from 'src/app/shared/models/movie.model';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'media-movies-add',
  templateUrl: './movies-add.component.html',
  styleUrls: ['./movies-add.component.css']
})
export class MoviesAddComponent implements OnInit {

  public movies: Movie[];
  public addedMovie: Movie;

  constructor(private router: Router, private moviesService: MoviesService) { }

  ngOnInit(): void {
  }

  public onClickSearchButton(form: NgForm){
    this.moviesService.lookupMovie(form.value.title).subscribe(data => this.movies = data);
  }

  public onClickAddMovie(onlineId: string){
      this.moviesService
          .addMovie(onlineId)
          .subscribe(
            data => {
                      this.addedMovie = data;
                      this.router.navigate(['/movies/', this.addedMovie.id]);
                     });
  }

  public validateTitleInput(title: NgModel){
    return {'red lighten-1': title.invalid && title.touched, 'green lighten-2': title.valid};
  }

  public showCharacterStringPluralOrNot(title: NgModel){
    if ( (title.errors?.minlength.actualLength) === 1){
        return 'character';
    }
    else{return 'characters'; }
  }

}
