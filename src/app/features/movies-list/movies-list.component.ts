import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie.model';
import { MoviesService } from '../../core/services/movie-data/movies.service';

@Component({
  selector: 'media-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {

  private movies: Movie[];

  constructor(private moviesService: MoviesService){}

  ngOnInit(){
    this.moviesService
        .getMovies()
        .subscribe(data =>
          {
            this.movies = data;
            this.movies.sort(
              (a, b) => a.title.toLocaleLowerCase().localeCompare(b.title.toLocaleLowerCase()));
          }
        );
  }

  public getMovies(): Movie[]{
      return this.movies;
  }

}
