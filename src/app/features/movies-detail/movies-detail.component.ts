import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { MoviesService } from '../../core/services/movie-data/movies.service';
import { Movie } from 'src/app/shared/models/movie.model';

@Component({
  selector: 'media-movies-detail',
  templateUrl: './movies-detail.component.html',
  styleUrls: ['./movies-detail.component.css']
})
export class MoviesDetailComponent implements OnInit {

  movie: Movie;
  constructor(private router: Router, private route: ActivatedRoute, private moviesService: MoviesService ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.moviesService.getMovie(parseInt(id, 10)).subscribe(data => this.movie = data);
  }

  /* public getMovie(): Movie{
    return this.movie;
  } */

  public onClickDelete(){
    this.moviesService.deleteMovie(this.movie.id).subscribe(() =>
      {
        this.router.navigate(['/movies']);
      }
    );
  }
}
