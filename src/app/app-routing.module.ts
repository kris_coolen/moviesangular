import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesListComponent } from './features/movies-list/movies-list.component';
import { MoviesDetailComponent } from './features/movies-detail/movies-detail.component';
import { MoviesAddComponent } from './features/movies-add/movies-add.component';
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {path: 'movies', component: MoviesListComponent},
  {path: 'movies/add', component: MoviesAddComponent},
  {path: 'movies/:id', component: MoviesDetailComponent}
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
