import { Injectable } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) { }

  public getMovies(): Observable<Movie[]>{
    return this.http.get<Movie[]>(environment.apiUrl);
  }

  public getMovie(id: number): Observable<Movie>{
    return this.http.get<Movie>(environment.apiUrl + '/' + id);
  }

  public lookupMovie(title: string): Observable<Movie[]>{
    return this.http.get<Movie[]>(environment.apiUrl + '/search',
     {params: new HttpParams().set('title', title)});
  }

  public addMovie(onlineId: string): Observable<Movie>{
    return this.http.post<Movie>(environment.apiUrl, { apiId: onlineId});
  }

  public deleteMovie(id: number): Observable<{}>{
    return this.http.delete(environment.apiUrl + '/' + id);
  }
}
