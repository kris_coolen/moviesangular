import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'media-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public firstName = 'Kris';
  public lastName = 'Coolen';
  public date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
